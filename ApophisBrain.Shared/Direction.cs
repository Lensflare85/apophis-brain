﻿using System;

namespace ApophisBrain.Shared {
    public enum Direction {
        None,
        Up,
        Right,
        Down,
        Left,
    }

    public static class Direction_to_IntVector2 {
        public static IntVector2 ToIntVector2(this Direction direction) {
            return
                direction == Direction.Up ? new IntVector2(0, -1) :
                direction == Direction.Down ? new IntVector2(0, 1) :
                direction == Direction.Right ? new IntVector2(1, 0) :
                direction == Direction.Left ? new IntVector2(-1, 0) :
                new IntVector2(0, 0);
        }
    }

    public static class IntVector2_to_Direction {
        public static Direction ToDirection(this IntVector2 vector, bool preferX = false) {
            if (vector.X == 0 && vector.Y == 0) {
                return Direction.None;
            } else {
                if (Math.Abs(vector.X) < Math.Abs(vector.Y)) {
                    return vector.Y < 0 ? Direction.Up : Direction.Down;
                } else if (Math.Abs(vector.X) > Math.Abs(vector.Y)) {
                    return vector.X < 0 ? Direction.Left : Direction.Right;
                } else {
                    if (preferX) {
                        return vector.X < 0 ? Direction.Left : Direction.Right;
                    } else {
                        return vector.Y < 0 ? Direction.Up : Direction.Down;
                    }
                }
            }
        }
    }
}
