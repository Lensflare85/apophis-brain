﻿using System.Collections.Generic;

namespace ApophisBrain.Shared {
    public static class DictionaryExtension {
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key) {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }
    }
}
