﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApophisBrain.Shared {
    public abstract class EntityInfo {
        public readonly Team Team;
        public readonly bool Alive;
        public readonly int Hitpoints;
        public readonly IntVector2 Location;

        public EntityInfo(Team team, bool alive, int hitpoints, IntVector2 location) {
            Team = team;
            Alive = alive;
            Hitpoints = hitpoints;
            Location = location;
        }
    }

    public class GemInfo : EntityInfo {
        public GemInfo(Team team, bool alive, int hitpoints, IntVector2 location) : 
            base(team, alive, hitpoints, location) {
        }
    }

    public class FighterInfo : EntityInfo {
        public readonly string Name;

        public FighterInfo(string name, Team team, bool alive, int hitpoints, IntVector2 location) :
            base(team, alive, hitpoints, location) {
            Name = name;
        }
    }

    public class TeamFighterInfo : FighterInfo {
        public readonly int WeaponEnergy;
        public readonly Direction LookingDirection;
        public readonly EntityInfo[] SeesEntities;
        public readonly Direction ShootingDirection;

        public TeamFighterInfo(string name, Team team, bool alive, int hitpoints, IntVector2 location, int weaponEnergy, Direction lookingDirection, EntityInfo[] seesEntities, Direction shootingDirection) : 
            base(name, team, alive, hitpoints, location) {
            WeaponEnergy = weaponEnergy;
            LookingDirection = lookingDirection;
            SeesEntities = seesEntities;
            ShootingDirection = shootingDirection;
        }
    }
}
