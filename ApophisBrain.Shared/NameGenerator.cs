﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApophisBrain.Shared {
    public class NameGenerator {
        private RandomNumberGenerator random;

        public NameGenerator(RandomNumberGenerator random) {
            this.random = random;
        }

        public string Next(int maxLength) {
            const int minLength = 3;
            var abstractWord = MakeAbstractWord(minLength + random.Next(maxLength - minLength));
            return MakeNameFromAbstractWord(abstractWord);
        }

        private string MakeNameFromAbstractWord(LetterType[] abstractWord) {
            StringBuilder sb = new StringBuilder();
            char lastLetter = ' ';
            for (int i=0; i< abstractWord.Length; ++i) {
                var letterType = abstractWord[i];
                char letter = ' ';
                int tryCounter = 100;
                if (letterType == LetterType.ShortConsonant) {
                    
                    do {
                        letter = shortConsonants[random.Next(shortConsonants.Length)];
                        --tryCounter;
                    } while (!CanBePairs(lastLetter, letter) && tryCounter > 0);
                } else if (letterType == LetterType.LongConsonant) {
                    do {
                        letter = longConsonants[random.Next(longConsonants.Length)];
                        --tryCounter;
                    } while (!CanBePairs(lastLetter, letter) && tryCounter > 0);
                } else {
                    do {
                        letter = vocals[random.Next(vocals.Length)];
                        --tryCounter;
                    } while (!CanBePairs(lastLetter, letter) && tryCounter > 0);
                }

                lastLetter = letter;

                if (i == 0) {
                    sb.Append(Char.ToUpper(letter));
                } else {
                    sb.Append(letter);
                }
            }
            return sb.ToString();
        }

        private LetterType[] MakeAbstractWord(int length) {
            var abstractWord = new LinkedList<LetterType>();

            for (int i = 0; i < length; ++i) {
                if (i == 2 && LastLetterTypes(abstractWord, 1, LetterType.ShortConsonant, LetterType.LongConsonant)) {
                    abstractWord.RemoveFirst();
                    abstractWord.AddFirst(LetterType.Vocal);
                }

                if (LastLetterTypes(abstractWord, 3, LetterType.Vocal)) {
                    abstractWord.AddLast(PickRandomLetterType(LetterType.ShortConsonant, LetterType.LongConsonant));
                }/* else if (LastLetterTypes(abstractWord, 3, LetterType.ShortConsonant, LetterType.LongConsonant)) {
                    abstractWord.AddLast(LetterType.Vocal);
                }*/ else if (LastLetterTypes(abstractWord, 2, LetterType.Vocal)) {
                    abstractWord.AddLast(PickRandomLetterType(LetterType.ShortConsonant, LetterType.LongConsonant, LetterType.Vocal, LetterType.ShortConsonant, LetterType.LongConsonant, LetterType.ShortConsonant, LetterType.LongConsonant));
                } else if (LastLetterTypes(abstractWord, 2, LetterType.ShortConsonant, LetterType.LongConsonant)) {
                    abstractWord.AddLast(LetterType.Vocal);
                } else if (LastLetterTypes(abstractWord, 1, LetterType.Vocal)) {
                    abstractWord.AddLast(PickRandomLetterType(LetterType.ShortConsonant, LetterType.LongConsonant, LetterType.ShortConsonant, LetterType.LongConsonant, LetterType.Vocal));
                } else {
                    abstractWord.AddLast(PickRandomLetterType(LetterType.ShortConsonant, LetterType.LongConsonant, LetterType.Vocal, LetterType.Vocal));
                }
            }

            if (LastLetterTypes(abstractWord, 2, LetterType.ShortConsonant, LetterType.LongConsonant)) {
                abstractWord.AddLast(LetterType.Vocal);
            }

            return abstractWord.ToArray();
        }

        private bool LastLetterTypes(LinkedList<LetterType> source, int numberOfPreviousLetters, params LetterType[] letterTypes) {
            var sourceArray = source.ToArray();
            int matches = 0;
            for (int letter = 0; letter < numberOfPreviousLetters; ++letter) {
                int i = sourceArray.Length - 1 - letter;
                if (i >= 0) {
                    var currentLetterType = sourceArray[i];
                    for (int letterTypeIndex=0; letterTypeIndex<letterTypes.Length; ++letterTypeIndex) {
                        if (currentLetterType == letterTypes[letterTypeIndex]) {
                            matches += 1;
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            return matches >= numberOfPreviousLetters;
        }

        private LetterType PickRandomLetterType(params LetterType[] letterTypes) {
            return letterTypes[random.Next(letterTypes.Length)];
        }

        private static readonly char[] vocals = new char[] {
            'a', 'e', 'i', 'o', 'u', 'y',
            'a', 'e', 'i', 'o', 'u',
        };

        private static readonly char[] shortConsonants = new char[] {
            'b','c','d','g','k','p','q','t','x',
            'b',    'd','g','k','p'    ,'t',
                    'd','g','k','p'    ,'t',
        };

        private static readonly char[] longConsonants = new char[] {
            'f','h','j','l','m','n','r','s','v','w','z',
            'f','h',    'l','m','n','r','s',    'w',
            'f','h',    'l','m','n','r','s',
        };

        bool CanBePairs(char c1, char c2) {
            for(int i=0; i<forbiddenPairs.Length; ++i) {
                var pair = forbiddenPairs[i].ToArray();
                if (c1 == pair[0] && c2 == pair[1]) {
                    return false;
                }
            }
            return true;
        }

        private static readonly string[] forbiddenPairs = {
            "yy", 
            "yi", "iy",
            "yj", "jy",

            "uy",

            "ii",
            "ij", "ji",
            
            "uu",

            "bp", "pb",
            "bg", "gb",
            "bk", "kb",
            "bq", "qb",
            "bt", "tb",
            "bw", "wb",
            "bv", "vb",

            "cc",

            "dt", "td",
            "dx",

            "fv", "vf",
            "fw", "wf",

            "gk", "kg",
            "gc", "cg",
            "gq", "qg",

            "hh",

            "jj",
            "jb", "bj",
            "jq", "qj",

            "kk",

            "pw", "wp",
            "pf", "fp",
            "pv", "vp",

            "qc", "cq",
            "qk", "kq",
            "qd", "dq",
            "qq",

            "tx",

            "vw", "wv",

            "vv",

            "ww",

            "xk", "kx",
            "xq", "qx",
            "xw", "wx",
            "xv", "vx",
            "xf", "fx",
            "xz", "zx",
            "xb", "bx",
            "xj", "jx",
            "xs", "sx",
            "xg", "gx",
            "xh", "hx",
            "xc", "cx",

            "zc", "cz",
            "zs", "sz",
        };

        enum LetterType {
            Vocal, ShortConsonant, LongConsonant,
        }
    }
}
