﻿using System.Collections.Generic;
using System.Linq;

namespace ApophisBrain.Shared {
    public static class E {
        public static IEnumerable<T> GetValues<T>() {
            return System.Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}
