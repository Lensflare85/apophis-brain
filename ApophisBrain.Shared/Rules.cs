﻿
namespace ApophisBrain.Shared {
    public struct Rules {
        public int FieldRows;
        public int FieldColumns;

        public int NumberOfWalls;

        public int FightersPerTeam;

        public int GemHitpoints;
        public int FighterHitpoints;

        public int WeaponEnergy;
        public int WeaponEnergyDrainedPerShot;
        public int WeaponEnergyChargedPerTurn;
        public int WeaponDamagePerShot;

        public int RangeOfSight;
    }
}
