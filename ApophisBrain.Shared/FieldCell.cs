﻿
namespace ApophisBrain.Shared {
    public enum FieldCell : int {
        None = 0, //everything outside the field bounds
        Ground = 1,
        Wall = 2,
    }
}
