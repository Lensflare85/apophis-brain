﻿using System;

namespace ApophisBrain.Shared {
    public class Map<T> {
        private readonly T[] cells;
        private readonly T outsideCell;

        public readonly int Columns;
        public readonly int Rows;

        private Map(int columns, int rows) {
            Columns = columns;
            Rows = rows;

            cells = new T[Columns * Rows];
        }

        public Map(int columns, int rows, T initialCell, T outsideCell) : this(columns, rows) {
            this.outsideCell = outsideCell;

            for (int i = 0; i < Columns * Rows; ++i) {
                cells[i] = initialCell;
            }
        }

        public Map<T> Clone() {
            var map = new Map<T>(Columns, Rows);
            Array.Copy(cells, map.cells, cells.Length);
            return map;
        }

        public T GetCell(int x, int y) {
            if (x >= 0 && y >= 0 && x < Columns && y < Rows) {
                return cells[x + y * Columns];
            } else {
                return outsideCell;
            }
        }

        public T GetCell(IntVector2 location) {
            return GetCell(location.X, location.Y);
        }

        public void SetCell(int x, int y, T cell) {
            if (x >= 0 && y >= 0 && x < Columns && y < Rows) {
                cells[x + y * Columns] = cell;
            }
        }

        public void SetCell(IntVector2 position, T cell) {
            SetCell(position.X, position.Y, cell);
        }

        public T this[int x, int y] {
            get {
                return GetCell(x, y);
            } set {
                SetCell(x, y, value);
            }
        }

        public T this[IntVector2 coordinates]
        {
            get
            {
                return GetCell(coordinates);
            }
            set
            {
                SetCell(coordinates, value);
            }
        }

    }
}
