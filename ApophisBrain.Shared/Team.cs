﻿
namespace ApophisBrain.Shared {
    public enum Team {
        Left, //The team that starts on the left side.
        Right, //The team that starts on the right side.
    }
}
