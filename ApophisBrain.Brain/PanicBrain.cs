﻿using System;
using ApophisBrain.Shared;

namespace ApophisBrain.BrainImplementation {
    public class PanicBrain : Brain {

        public override void NewMatchDidStart() {

        }

        public override Map<string> NewTurnDidStart() {
            return null;
        }

        public override string GetName() {
            return "Panic";
        }

        public override string GetVersion() {
            return "1.0";
        }

        public override Direction NextTurnFighterLook(int fighterIndex) {
            return PickRandomDirection(Direction.Left, Direction.Right, Direction.Up, Direction.Down);
        }

        public override Direction NextTurnFighterMove(int fighterIndex) {
            return PickRandomDirection(Direction.Left, Direction.Right, Direction.Up, Direction.Down);
        }

        public override Direction NextTurnFighterShoot(int fighterIndex) {
            return PickRandomDirection(Direction.Left, Direction.Right, Direction.Up, Direction.Down, Direction.None, Direction.None, Direction.None, Direction.None);
        }

        Direction PickRandomDirection(params Direction[] possibleDirections) {
            return possibleDirections[Random.Next(possibleDirections.Length)];
        }
    }
}
