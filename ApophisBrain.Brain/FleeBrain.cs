﻿using System;
using ApophisBrain.Shared;

namespace ApophisBrain.BrainImplementation {
    public class FleeBrain : Brain {

        public override void NewMatchDidStart() {

        }

        public override Map<string> NewTurnDidStart() {
            return null;
        }

        public override string GetName() {
            return "Flee";
        }

        public override string GetVersion() {
            return "1.0";
        }

        public override Direction NextTurnFighterLook(int fighterIndex) {
            if (Team == Team.Right) {
                return PickRandomDirection(Direction.Left);
            } else {
                return PickRandomDirection(Direction.Right);
            }
        }

        public override Direction NextTurnFighterMove(int fighterIndex) {
            if (Team == Team.Right) {
                return PickRandomDirection(Direction.Up, Direction.Down, Direction.Right, Direction.Right);
            } else {
                return PickRandomDirection(Direction.Up, Direction.Down, Direction.Left, Direction.Left);
            }
        }

        public override Direction NextTurnFighterShoot(int fighterIndex) {
            if (Team == Team.Right) {
                return PickRandomDirection(Direction.Left, Direction.None); 
            } else {
                return PickRandomDirection(Direction.Right, Direction.None);
            }
        }

        Direction PickRandomDirection(params Direction[] possibleDirections) {
            return possibleDirections[Random.Next(possibleDirections.Length)];
        }
    }
}
