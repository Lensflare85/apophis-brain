﻿using ApophisBrain.Shared;

namespace ApophisBrain.BrainImplementation {
    public abstract class Brain {

        protected RandomNumberGenerator Random { get; private set; }

        protected Team Team { get; private set; }
        protected int Turn { get; private set; }

        protected Rules Rules { get; private set; }

        protected IntVector2 TeamGemLocation { get; private set; }
        protected IntVector2 EnemyGemLocation { get; private set; }

        protected TeamFighterInfo[] TeamFighterInfos { get; private set; }

        protected Map<FieldCell> FieldMap { get; private set; }

        public void NotifyNewMatch(Team team, Rules rules, RandomNumberGenerator random, IntVector2 teamGemLocation, IntVector2 enemyGemLocation, Map<FieldCell> fieldMap) {
            Team = team;
            Rules = rules;
            Random = random;
            TeamGemLocation = teamGemLocation;
            EnemyGemLocation = enemyGemLocation;
            FieldMap = fieldMap;

            NewMatchDidStart();
        }

        public abstract void NewMatchDidStart();

        public Map<string> NotifyNewTurn(int turn, TeamFighterInfo[] teamFighterInfos) {
            Turn = turn;
            TeamFighterInfos = teamFighterInfos;

            return NewTurnDidStart();
        }

        public abstract Map<string> NewTurnDidStart();

        public abstract string GetName();

        public abstract string GetVersion();

        public abstract Direction NextTurnFighterLook(int fighterIndex);

        public abstract Direction NextTurnFighterMove(int fighterIndex);

        public abstract Direction NextTurnFighterShoot(int fighterIndex);
    }
}
