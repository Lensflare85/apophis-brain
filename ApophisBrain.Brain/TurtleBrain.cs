﻿using System;
using ApophisBrain.Shared;

namespace ApophisBrain.BrainImplementation {
    public class TurtleBrain : Brain {

        public override void NewMatchDidStart() {

        }

        public override Map<string> NewTurnDidStart() {
            return null;
        }

        public override string GetName() {
            return "Turtle";
        }

        public override string GetVersion() {
            return "1.0";
        }

        public override Direction NextTurnFighterLook(int fighterIndex) {
            var fighter = TeamFighterInfos[fighterIndex];
            var gemToFighter = fighter.Location - TeamGemLocation;
            var lookDirection = gemToFighter.ToDirection(Random.Next(2) == 0);
            return lookDirection;
        }

        public override Direction NextTurnFighterMove(int fighterIndex) {
            if (Random.Next(7) == 0) {
                return PickRandomDirection(Direction.Left, Direction.Right, Direction.Up, Direction.Down, Direction.None);
            }

            var fighter = TeamFighterInfos[fighterIndex];
            var fighterToGem = TeamGemLocation - fighter.Location;
            return fighterToGem.ToDirection(Random.Next(2) == 0);
        }

        public override Direction NextTurnFighterShoot(int fighterIndex) {
            var fighter = TeamFighterInfos[fighterIndex];
            var gemToFighter = fighter.Location - TeamGemLocation;
            var shootDirection = PickRandomDirection(Direction.Left, Direction.Right, Direction.Up, Direction.Down);
            var checkLocation = fighter.Location;
            var checkDistance = 0;
            while(true) {
                checkLocation += shootDirection.ToIntVector2();
                checkDistance += 1;
                if (FieldMap[checkLocation] != FieldCell.Ground) {
                    if (checkDistance == 1) {
                        return Direction.None; //there is a wall right in front of the shoot direction. Don't shoot!
                    } else {
                        return shootDirection; //There is a wall and no fighters of my team are in the way. shoot!
                    }
                }
                foreach (var teamFighter in TeamFighterInfos) {
                    if (teamFighter.Alive && teamFighter.Location == checkLocation || TeamGemLocation == checkLocation) {
                        return Direction.None; //One of my living figthers or my gem is in the way for this turn. Don't shoot!
                    }
                }
            }
        }

        Direction PickRandomDirection(params Direction[] possibleDirections) {
            return possibleDirections[Random.Next(possibleDirections.Length)];
        }
    }
}
