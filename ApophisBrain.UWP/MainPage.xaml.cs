﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using ApophisBrain.Logic;
using ApophisBrain.Shared;
using System.Collections.Generic;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ApophisBrain.UWP {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page {
        Game game;
        Renderer renderer = new Renderer();
        bool nextSliderChangeIsFromCode = false;
        Random seedGenerator = new Random();

        SolidColorBrush blackColorBrush = new SolidColorBrush(Colors.Black);
        Dictionary<Team, SolidColorBrush> teamColorBrushes;

        public MainPage() {
            this.InitializeComponent();

            infoToolTip.Visibility = Visibility.Collapsed;

            teamColorBrushes = new Dictionary<Team, SolidColorBrush> {
                { Team.Left, new SolidColorBrush(renderer.TeamColors[Team.Left]) },
                { Team.Right, new SolidColorBrush(renderer.TeamColors[Team.Right]) },
            };

            CreateAndStartGame();
        }

        void CreateAndStartGame() {
            if (game != null) {
                game.Stop();
                game.UpdateUI = null;
                game.UpdateUIForNewTurn = null;
            }

            game = new Game();

            ApplyRandomSeed();

            game.UpdateUI = () => {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                    UpdateUIForTick();
                });
            };
            game.UpdateUIForNewTurn = () => {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                    nextSliderChangeIsFromCode = true;
                    turnSlider.Value = game.Match.CurrentTurn;
                });
            };
            game.BrainDidWin = (brain) => {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                    UpdateScore();
                });
            };
            game.Start();
        }

        void StartNewMatch(int seed) {
            Debug.WriteLine($"seed of new match: {seed}");

            game.StartNewMatch(seed);

            UpdateTurnDuration();

            var leftBrain = game.Brains[Team.Left];
            var rightBrain = game.Brains[Team.Right];

            teamLeftTextBlock.Text = $"{leftBrain.GetName()} {leftBrain.GetVersion()}";
            teamRightTextBlock.Text = $"{rightBrain.GetName()} {rightBrain.GetVersion()}";

            teamLeftTextBlock.Foreground = teamColorBrushes[Team.Left];
            teamRightTextBlock.Foreground = teamColorBrushes[Team.Right];

            winnerTextBlock.Text = "";

            infoToolTip.Content = game.MakeRulesInfoText();

            UpdateScore();
        }

        void UpdateUIForTick() {
            canvas.Invalidate();

            turnValueLabel.Text = game.Match.CurrentTurn.ToString();
            turnSlider.Maximum = game.Match.History.Count - 1;

            playOrPauseButtonSymbol.Symbol = game.IsPaused ? Symbol.Play : Symbol.Pause;

            var loserTeams = game.Match.LoserTeams;
            if (loserTeams.Any()) {
                if (loserTeams.Count > 1) {
                    winnerTextBlock.Text = "Draw!";
                    winnerTextBlock.Foreground = blackColorBrush;
                } else {
                    var winnerTeam = loserTeams.Contains(Team.Left) ? Team.Right : Team.Left;
                    winnerTextBlock.Text = winnerTeam == Team.Left ? "<-- Winner" : "Winner -->";
                    winnerTextBlock.Foreground = teamColorBrushes[winnerTeam];
                }
            } else {
                winnerTextBlock.Text = "";
            }
        }

        void UpdateScore() {
            scoreBrain1TextBlock.Text = $"{game.BrainA.GetName()}: {game.ScoreBrainA}";
            scoreBrain2TextBlock.Text = $"{game.BrainB.GetName()}: {game.ScoreBrainB}";
        }

        void ApplySeed() {
            int seed = 0;
            int.TryParse(textBox.Text, out seed);

            textBox.Text = seed.ToString();

            StartNewMatch(seed);

            canvas.Invalidate();
        }

        void ApplyRandomSeed() {
            int seed = seedGenerator.Next();

            textBox.Text = seed.ToString();

            StartNewMatch(seed);

            canvas.Invalidate();
        }

        void UpdateTurnDuration() {
            turnDurationTextBox.Text = string.Format("{0:0.00}", game.TurnDuration);
        }

        private void canvasControl_Draw(CanvasControl sender, CanvasDrawEventArgs args) {
            renderer.Update(args.DrawingSession, new Vector2((float)sender.ActualWidth, (float)sender.ActualHeight), game);
            renderer.Render();
        }

        private void applySeedButton_Click(object sender, RoutedEventArgs e) {
            ApplySeed();
        }

        private void applyRandomSeedButton_Click(object sender, RoutedEventArgs e) {
            ApplyRandomSeed();
        }

        private void playOrPauseButton_Click(object sender, RoutedEventArgs e) {
            //TODO: encapsulate into a Game method
            game.IsPaused = !game.IsPaused;
            game.ReverseAnimation = false;
        }

        private void stepBackButton_Click(object sender, RoutedEventArgs e) {
            //TODO: encapsulate into a Game method
            game.Match.StepBack();
            game.IsPaused = true;
        }

        private void stepForwardButon_Click(object sender, RoutedEventArgs e) {
            //TODO: encapsulate into a Game method
            if (game.IsPaused) {
                if (game.ReverseAnimation) {
                    game.IsPaused = true;
                } else {
                    game.IsPaused = false;
                    game.ShouldPauseNextTurn = true;
                }
            } else {
                game.IsPaused = true;
            }
            game.ReverseAnimation = false;
        }

        private void turnSlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e) {
            if (!nextSliderChangeIsFromCode) {
                //TODO: encapsulate into a Game method
                game.Match.SetHistoryTurn((int)e.NewValue);
                game.SkipAnimation();
            } else {
                nextSliderChangeIsFromCode = false;
            }
        }

        private void canvas_PointerExited(object sender, PointerRoutedEventArgs e) {
            renderer.MousePosition = new Vector2(-1, -1);
        }

        private void canvas_PointerMoved(object sender, PointerRoutedEventArgs e) {
            renderer.MousePosition = e.GetCurrentPoint(canvas).Position.ToVector2();
        }

        private void textBox_KeyDown(object sender, KeyRoutedEventArgs e) {
            if (e.Key == VirtualKey.Enter) {
                ApplySeed();
            }
        }

        private void turnDurationSubtractButton_Click(object sender, RoutedEventArgs e) {
            game.DecreaseTurnDuration();
            UpdateTurnDuration();
        }

        private void turnDurationAddButton_Click(object sender, RoutedEventArgs e) {
            game.IncreaseTurnDuration();
            UpdateTurnDuration();
        }

        private void infoSymbolIcon_PointerEntered(object sender, PointerRoutedEventArgs e) {
            infoToolTip.Visibility = Visibility.Visible;
        }

        private void infoSymbolIcon_PointerExited(object sender, PointerRoutedEventArgs e) {
            infoToolTip.Visibility = Visibility.Collapsed;
        }

        private void teamLeftTextBlock_PointerEntered(object sender, PointerRoutedEventArgs e) {
            renderer.showLeftTeamBrainInfoMap = true;
        }

        private void teamLeftTextBlock_PointerExited(object sender, PointerRoutedEventArgs e) {
            renderer.showLeftTeamBrainInfoMap = false;
        }

        private void teamRightTextBlock_PointerEntered(object sender, PointerRoutedEventArgs e) {
            renderer.showRightTeamBrainInfoMap = true;
        }

        private void teamRightTextBlock_PointerExited(object sender, PointerRoutedEventArgs e) {
            renderer.showRightTeamBrainInfoMap = false;
        }
    }
}
