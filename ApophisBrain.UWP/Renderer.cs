﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.Text;
using Windows.UI;
using ApophisBrain.Shared;
using ApophisBrain.Logic;

namespace ApophisBrain.UWP {
    public class Renderer {
        public CanvasDrawingSession DrawingSession { get; private set; }
        public Vector2 CanvasSize { get; private set; }

        Game game;

        public Vector2 MousePosition = new Vector2(-1, -1);

        public bool showLeftTeamBrainInfoMap = false;
        public bool showRightTeamBrainInfoMap = false;

        const byte backroundColorBrightness = (byte)150;
        Color backgroundColor = new Color() { R = backroundColorBrightness, G = backroundColorBrightness, B = backroundColorBrightness, A = 255 };
        Color wallColor = Colors.DarkOliveGreen;
        Color grassColor = Colors.LightGreen;
        Color darkTransparentBackgroundColor = new Color() { R = 0, G = 0, B = 0, A = 100 };
        Color brightTransparentBackgroundColor = new Color() { R = 255, G = 255, B = 255, A = 210 };
        Color infoMapTextColor = Colors.Yellow;

        public Dictionary<Team, Color> TeamColors = new Dictionary<Team, Color> {
            { Team.Left, Colors.Red },
            { Team.Right, Colors.Blue },
        };

        const float damageAnimationThreshold = 0.9f;

        CanvasTextFormat textFormat = new CanvasTextFormat() {
            FontSize = 18,
        };

        int numberOfCellsX;
        int numberOfCellsY;
        Vector2 fieldSize;
        Vector2 cellSize;
        Vector2 fieldOffset;
        Vector2 constOffset;

        int currentTurn;
        float easeProgress;

        List<Tuple<Fighter, Team>> panelsToDrawForFighters = new List<Tuple<Fighter, Team>>();

        public void Update(CanvasDrawingSession drawingSession, Vector2 canvasSize, Game game) {
            DrawingSession = drawingSession;
            CanvasSize = canvasSize;
            this.game = game;

            DrawingSession.Antialiasing = CanvasAntialiasing.Antialiased;
            DrawingSession.TextAntialiasing = CanvasTextAntialiasing.ClearType;

            numberOfCellsX = game.Rules.FieldColumns;
            numberOfCellsY = game.Rules.FieldRows;

            var cellRatio = numberOfCellsX / (float)numberOfCellsY;

            var canvasWidth = CanvasSize.X;
            var canvasHeight = CanvasSize.Y;

            var canvasRatio = canvasWidth / canvasHeight;

            float fieldWidth;
            float fieldHeight;
            if (cellRatio > canvasRatio) {
                fieldWidth = canvasWidth;
                fieldHeight = (canvasWidth / cellRatio);
            } else {
                fieldWidth = (canvasHeight * cellRatio);
                fieldHeight = canvasHeight;
            }

            fieldSize = new Vector2(fieldWidth, fieldHeight);
            fieldOffset = new Vector2(canvasWidth * 0.5f - fieldWidth * 0.5f, canvasHeight * 0.5f - fieldHeight * 0.5f);

            var cellWidth = (float)(fieldWidth / numberOfCellsX);
            var cellHeight = (float)(fieldHeight / numberOfCellsY);
            cellSize = new Vector2(cellWidth, cellHeight);

            var halfCellSize = cellSize * 0.5f;

            constOffset = Vector2.One * 0.5f + fieldOffset + halfCellSize;

            currentTurn = game.Match.CurrentTurn;
            easeProgress = game.AnimationEaseProgress();
        }

        public void Render() {
            DrawingSession.Clear(backgroundColor);
            DrawField();
            DrawGems();
            DrawFighters();
            DrawSightLines();
            DrawShots();
            DrawInfoPanels();
            DrawHoveredCoordinates();
        }

        void DrawField() {
            //cells:
            for (int y = 0; y < numberOfCellsY; ++y) {
                for (int x = 0; x < numberOfCellsX; ++x) {
                    var rectX = (float)(x * cellSize.X) + fieldOffset.X;
                    var rectY = (float)(y * cellSize.Y) + fieldOffset.Y;

                    var cellColor = game.Match.Field.IsPassable(x, y) ? grassColor : wallColor;
                    DrawingSession.FillRectangle(rectX, rectY, cellSize.X, cellSize.Y, cellColor);
                }
            }

            //grid:
            for (int y = 0; y < numberOfCellsY + 1; ++y) {
                for (int x = 0; x < numberOfCellsX + 1; ++x) {
                    var vLineX = (float)(x * cellSize.X) + fieldOffset.X;
                    var hLineY = (float)(y * cellSize.Y) + fieldOffset.Y;

                    float offset = 0.5f;
                    DrawingSession.DrawLine(offset + (int)vLineX, offset + (int)fieldOffset.Y, offset + (int)vLineX, offset + (int)fieldOffset.Y + (int)fieldSize.Y - 1, backgroundColor);
                    DrawingSession.DrawLine(offset + (int)fieldOffset.X, offset + (int)hLineY, offset + (int)fieldOffset.X + (int)fieldSize.X - 1, offset + (int)hLineY, backgroundColor);
                }
            }
        }

        void DrawGems() {
            Func<Team, Vector2> gemOffset = (team) => {
                var gem = game.Match.Gems[team];
                var gemLocation = gem.Location.Vector2;

                if (currentTurn > 0) {
                    Gem previousGem = game.Match.History[currentTurn - 1].Gems[team];
                    gemLocation = Vector2.Lerp(previousGem.Location.Vector2, gem.Location.Vector2, easeProgress);
                }

                return constOffset + gemLocation * cellSize;
            };

            Func<Team, float> animatedGemHpQuotient = (team) => {
                float gemMaxHp = game.Rules.GemHitpoints;
                int hp = game.Match.Gems[team].HitPoints;
                if (currentTurn > 0) {
                    var oneTurnBack = game.Match.History[currentTurn - 1];
                    int previousHp = oneTurnBack.Gems[team].HitPoints;
                    if (hp != previousHp) {
                        return (easeProgress > damageAnimationThreshold ? hp : previousHp) / gemMaxHp;
                    } else {
                        return hp / gemMaxHp;
                    }
                } else {
                    return hp / gemMaxHp;
                }
            };

            var gemRadius = cellSize.X * 0.47f - 1;

            var points = new Vector2[] {
                new Vector2(0, gemRadius),
                new Vector2(gemRadius, 0),
                new Vector2(0, -gemRadius),
                new Vector2(-gemRadius, 0),
            };

            foreach (var team in game.Teams) {
                var canvasPosition = gemOffset(team);

                var hpQuotient = animatedGemHpQuotient(team);

                var color = TeamColors[team];

                var colorWithAlpha = color;
                colorWithAlpha.A = ByteFromInt((int)(hpQuotient * 255));

                DrawingSession.FillGeometry(CanvasGeometry.CreatePolygon(DrawingSession, points), canvasPosition, colorWithAlpha);
                DrawingSession.DrawGeometry(CanvasGeometry.CreatePolygon(DrawingSession, points), canvasPosition, color);
            }
        }

        void DrawFighters() {
            Func<Team, int, Vector2> fighterOffset = (team, fighterIndex) => {
                var fighter = game.Match.Fighters[team][fighterIndex];
                var fighterLocation = fighter.Location.Vector2;

                if (currentTurn > 0) {
                    Fighter previousFighter = game.Match.History[currentTurn - 1].Fighters[team][fighterIndex];
                    fighterLocation = Vector2.Lerp(previousFighter.Location.Vector2, fighter.Location.Vector2, easeProgress);
                }

                return constOffset + fighterLocation * cellSize;
            };

            Func<int, Team, float> animatedFighterHpQuotient = (fighterIndex, team) => {
                float fighterMaxHp = game.Rules.FighterHitpoints;
                int hp = game.Match.Fighters[team][fighterIndex].HitPoints;
                if (currentTurn > 0) {
                    var oneTurnBack = game.Match.History[currentTurn - 1];
                    int previousHp = oneTurnBack.Fighters[team][fighterIndex].HitPoints;
                    if (hp != previousHp) {
                        return (easeProgress > damageAnimationThreshold ? hp : previousHp) / fighterMaxHp;
                    } else {
                        return hp / fighterMaxHp;
                    }
                } else {
                    return hp / fighterMaxHp;
                }
            };

            var fighterRadius = cellSize.X * 0.4f - 1;

            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                foreach (var team in game.Teams) {
                    var fighterCanvasPosition = fighterOffset(team, i);

                    var hpQuotient = animatedFighterHpQuotient(i, team);

                    var color = TeamColors[team];
                    var colorWithAlpha = color;
                    colorWithAlpha.A = ByteFromInt((int)(hpQuotient * 255));

                    DrawingSession.FillCircle(fighterCanvasPosition, fighterRadius, colorWithAlpha);
                    DrawingSession.DrawCircle(fighterCanvasPosition, fighterRadius, color);
                }
            }

            //TODO: animate small step forth and back if there was a move request but the target location is not passable.
        }

        void DrawSightLines() {
            const float sightAnimationTheashold = 0.7f;
            Func<int, Team, Tuple<Vector2, Vector2>> sightDrawPositions = (fighterIndex, team) => {
                var fighter = game.Match.Fighters[team][fighterIndex];

                if (fighter.LookingDirection != Direction.None) {
                    if (currentTurn > 0 && easeProgress > sightAnimationTheashold) {
                        var pCurrent = fighter.Location;
                        var p2Current = fighter.EndOfSightLocation;

                        var lastTurn = game.Match.History[currentTurn - 1];

                        var pLast = lastTurn.Fighters[team][fighterIndex].Location;
                        var pDiff = pCurrent - pLast;
                        var p2Last = p2Current - pDiff;

                        var p = Vector2.Lerp(pCurrent.Vector2, pLast.Vector2, 1 - easeProgress);
                        var p2 = Vector2.Lerp(p2Current.Vector2, p2Last.Vector2, 1 - easeProgress);

                        return new Tuple<Vector2, Vector2>(CellDrawPosition(p), CellDrawPosition(p2));
                    }
                }

                return null;
            };

            foreach (var team in game.Teams) {
                for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                    var fighter = game.Match.Fighters[team][i];

                    var color = TeamColors[team];
                    color.A = 20;

                    var p = sightDrawPositions(i, team);
                    if (p != null) {
                        DrawingSession.DrawLine(p.Item1, p.Item2, color, cellSize.X * 0.75f);
                    }
                }
            }
        }

        void DrawShots() {
            const float shootAnimationTheashold = 0.7f;
            Func<int, Team, Tuple<Vector2, Vector2>> shotDrawPositions = (fighterIndex, team) => {
                var fighter = game.Match.Fighters[team][fighterIndex];

                if (fighter.ShootingDirection != Direction.None) {
                    if (currentTurn > 0 && easeProgress > shootAnimationTheashold) {
                        var pCurrent = fighter.Location;
                        var p2Current = fighter.HitLocation.Value;

                        var lastTurn = game.Match.History[currentTurn - 1];

                        var pLast = lastTurn.Fighters[team][fighterIndex].Location;
                        var pDiff = pCurrent - pLast;
                        var p2Last = p2Current - pDiff;

                        var p = Vector2.Lerp(pCurrent.Vector2, pLast.Vector2, 1 - easeProgress);
                        var p2 = Vector2.Lerp(p2Current.Vector2, p2Last.Vector2, 1 - easeProgress);

                        var directionDrawOffset =
                            fighter.ShootingDirection == Direction.Right ? new Vector2(0, 1) :
                            fighter.ShootingDirection == Direction.Left ? new Vector2(0, -1) :
                            fighter.ShootingDirection == Direction.Up ? new Vector2(1, 0) :
                            new Vector2(-1, 0);

                        return new Tuple<Vector2, Vector2>(CellDrawPosition(p) + directionDrawOffset, CellDrawPosition(p2) + directionDrawOffset);
                    }
                }

                return null;
            };

            //shots:
            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                foreach (var team in game.Teams) {
                    var fighter = game.Match.Fighters[team][i];

                    var c = TeamColors[team];

                    var p = shotDrawPositions(i, team);
                    if (p != null) {
                        DrawingSession.DrawLine(p.Item1, p.Item2, c);
                    }
                }
            }
        }

        void DrawInfoPanels() {
            panelsToDrawForFighters.Clear();

            foreach (var team in game.Teams) {
                var gem = game.Match.Gems[team];
                if (gem.Location == CellPositionFromCanvasPosition(MousePosition)) {
                    DrawGemInfoPanel(gem, team);
                }

                for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                    var fighter = game.Match.Fighters[team][i];
                    if (fighter.Location == CellPositionFromCanvasPosition(MousePosition)) {
                        panelsToDrawForFighters.Add(new Tuple<Fighter, Team>(fighter, team));
                    }
                }
            }

            foreach (var panelForFighter in panelsToDrawForFighters) {
                //if there is more than one fighter on the cell, draw only the living fighter:
                if (panelsToDrawForFighters.Count == 1 || panelForFighter.Item1.Alive) {
                    DrawFighterInfoPanel(panelForFighter.Item1, panelForFighter.Item2);
                    break;
                }
            }

            foreach (var team in game.Teams) {
                if ((team == Team.Left && showLeftTeamBrainInfoMap) || (team == Team.Right && showRightTeamBrainInfoMap)) {
                    for (int y = 0; y < numberOfCellsY + 1; ++y) {
                        for (int x = 0; x < numberOfCellsX + 1; ++x) {
                            string infoText = game.Match.InfoMaps[team]?[x, y];
                            if (infoText != null) {
                                DrawBrainInfoPanel(infoText, new IntVector2(x, y));
                            }
                        }
                    }
                }
            }
        }

        void DrawHoveredCoordinates() {
            var coords = CellPositionFromCanvasPosition(MousePosition);
            if (coords.HasValue) {
                var v = coords.Value;
                if (v.X >= 0 && v.Y >= 0) {
                    DrawInfoPanel($"{v.X},{v.Y}", 0, 0, 0, infoMapTextColor, darkTransparentBackgroundColor);
                }
            }
        }

        Vector2 CellDrawPosition(Vector2 cell) {
            return constOffset + cell * cellSize;
        }

        IntVector2? CellPositionFromCanvasPosition(Vector2 canvasPosition) {
            if (canvasPosition.X >= 0 && canvasPosition.Y >= 0) {
                var cellPosition = new IntVector2((canvasPosition - fieldOffset) / cellSize, v => (int)Math.Floor(v));
                if (cellPosition.X < game.Rules.FieldColumns && cellPosition.Y < game.Rules.FieldRows) {
                    return cellPosition;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }

        float Lerp(float a, float b, float f) {
            return a + f * (b - a);
        }

        byte ByteFromInt(int i) {
            return (byte)(i < 0 ? 0 : i > 255 ? 255 : i);
        }

        void DrawFighterInfoPanel(Fighter fighter, Team team) {
            var sb = new StringBuilder();
            sb.AppendLine(fighter.Name);
            sb.AppendLine($"{fighter.HitPoints}/{game.Rules.FighterHitpoints} HP");
            sb.AppendLine($"{fighter.WeaponEnergy}/{game.Rules.WeaponEnergy} Energy");
            var text = sb.ToString();

            DrawInfoPanel(text, fighter.Location, team);
        }

        void DrawGemInfoPanel(Gem gem, Team team) {
            var sb = new StringBuilder();
            sb.AppendLine($"{gem.HitPoints}/{game.Rules.GemHitpoints} HP");
            var text = sb.ToString();

            DrawInfoPanel(text, gem.Location, team);
        }

        void DrawBrainInfoPanel(string text, IntVector2 cellPosition) {
            DrawInfoPanel(text, 0, cellPosition, infoMapTextColor, darkTransparentBackgroundColor);
        }

        void DrawInfoPanel(string text, IntVector2 cellPosition, Team team) {
            var textColor = TeamColors[team];
            DrawInfoPanel(text, -cellSize.Y / 2, cellPosition, textColor, brightTransparentBackgroundColor);
        }

        void DrawInfoPanel(string text, float yOffset, IntVector2 cellPosition, Color textColor, Color backgroundColor) {
            var drawPosition = CellDrawPosition(cellPosition.Vector2);
            DrawInfoPanel(text, drawPosition.X, drawPosition.Y, yOffset, textColor, backgroundColor);
        }

        void DrawInfoPanel(string text, float x, float y, float yOffset, Color textColor, Color backgroundColor) {
            const float textPaddingX = 6f;
            const float textPaddingY = 2f;
            const float textPlateYOffset = 4f;

            var textLayout = new CanvasTextLayout(DrawingSession, text, textFormat, 1000, 1000);
            var textBounds = textLayout.DrawBounds;

            var textX = x - (float)textBounds.Width * 0.5f;
            var textY = y + yOffset + textPlateYOffset - (float)textBounds.Height;

            if (textX < textPaddingX * 2) {
                textX = textPaddingX * 2;
            }

            if (textY < textPaddingY * 2) {
                textY = textPaddingY * 2;
            }

            DrawingSession.FillRoundedRectangle(textX - textPaddingX, textY - textPaddingY, (float)textBounds.Width + textPaddingX * 2, (float)textBounds.Height + textPaddingY * 2, 3, 3, backgroundColor);
            DrawingSession.DrawText(text, textX - (float)textBounds.X, textY - (float)textBounds.Y, textColor, textFormat);
        }
    }
}
