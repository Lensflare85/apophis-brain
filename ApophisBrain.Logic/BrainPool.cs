﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ApophisBrain.Logic {
    public class BrainPool {
        public Type FirstBrainType { get; private set; }
        public Type SecondBrainType { get; private set; }

        public BrainPool() {
            FirstBrainType = LoadBrain("ApophisBrain.BrainImplementation", "PanicBrain");
            SecondBrainType = LoadBrain("ApophisBrain.BrainImplementation", "TurtleBrain");
        }

        Type LoadBrain(string assembly, string nameSpace, string type) {
            return Assembly.Load(new AssemblyName(assembly)).GetType(nameSpace + "." + type);
        }

        Type LoadBrain(string assemblyAndNameSpace, string type) {
            return LoadBrain(assemblyAndNameSpace, assemblyAndNameSpace, type);
        }
    }
}
