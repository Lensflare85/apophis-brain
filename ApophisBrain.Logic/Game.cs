﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using ApophisBrain.BrainImplementation;
using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public class Game {
        public RandomNumberGenerator Random { get; private set; }
        public Rules Rules { get; private set; }
        public Match Match;

        int seed;

        //TODO: add a logfile to record wins/losses and the seeds of the matches.

        public readonly Team[] Teams = { Team.Left, Team.Right };

        public bool IsPaused = true;
        public bool ShouldPauseNextTurn = false;
        public bool ReverseAnimation = false;

        const double minTurnDuration = 0.01;
        const double maxTurnDuration = 2.0;

        private Timer timer;
        private TimeSpan turnDuration;
        private TimeSpan interTurnDuration = TimeSpan.FromSeconds(0.1);
        private TimeSpan animationTimeProgress = TimeSpan.Zero;
        private DateTime currentTime;

        public double TurnDuration
        {
            get
            {
                return turnDuration.TotalSeconds;
            }
            set
            {
                turnDuration = TimeSpan.FromSeconds(value);
                interTurnDuration = TimeSpan.FromSeconds(value * 0.1);
            }
        }

        public BrainPool BrainPool = new BrainPool();

        public Brain BrainA;
        public Brain BrainB;

        public int ScoreBrainA = 0;
        public int ScoreBrainB = 0;

        public Dictionary<Team, Brain> Brains = new Dictionary<Team, Brain>();

        bool calculatingTick = false;

        public Game() {
            BrainA = (Brain)Activator.CreateInstance(BrainPool.FirstBrainType);
            BrainB = (Brain)Activator.CreateInstance(BrainPool.SecondBrainType);

            TurnDuration = 1;
        }

        int ValueWithAddedSmallVariance(int value) {
            return value + Random.Next(value / 3 + 1);
        }

        int ValueWithAddedBigVariance(int value) {
            return value + Random.Next(value * 2 + 1);
        }

        public string MakeRulesInfoText() {
            int fieldSize = Rules.FieldColumns * Rules.FieldRows;
            string wallRatio = string.Format("{0:0.00}", Rules.NumberOfWalls / (float)(fieldSize));

            var sb = new StringBuilder();

            sb.AppendLine($"--- Rules for seed {seed} ---");
            sb.AppendLine($"Field size: {Rules.FieldColumns} x {Rules.FieldRows} = {fieldSize}");
            sb.AppendLine($"Number of walls: {Rules.NumberOfWalls} (ratio: {wallRatio})");
            sb.AppendLine($"Fighters per team: {Rules.FightersPerTeam}");
            sb.AppendLine($"Gem HP: {Rules.GemHitpoints}");
            sb.AppendLine($"Fighter HP: {Rules.FighterHitpoints}");
            sb.AppendLine($"Fighter range of sight: {Rules.RangeOfSight}");
            sb.AppendLine($"Weapon energy: {Rules.WeaponEnergy}");
            sb.AppendLine($"Weapon energy consumed per shot: {Rules.WeaponEnergyDrainedPerShot}");
            sb.AppendLine($"Weapon energy charged per turn: {Rules.WeaponEnergyChargedPerTurn}");
            sb.Append($"Weapon damage per shot: {Rules.WeaponDamagePerShot}");

            return sb.ToString();
        }

        public void StartNewMatch(int seed) {
            this.seed = seed;
            Random = new RandomNumberGenerator(seed);

            int columns = ValueWithAddedSmallVariance(Settings.MinimumFieldColumns);
            int rows = ValueWithAddedSmallVariance(Settings.MinimumFieldRows);
            if (rows % 2 == 0) rows += 1;

            const int minimumNumberOfWalls = Settings.MinimumFieldColumns * Settings.MinimumFieldRows / 10;

            var rules = new Rules() {
                FieldColumns = columns,
                FieldRows = rows,
                NumberOfWalls = ValueWithAddedBigVariance(minimumNumberOfWalls),

                FightersPerTeam = ValueWithAddedSmallVariance(Settings.MinimumFightersPerTeam),

                FighterHitpoints = ValueWithAddedSmallVariance(Settings.MinimumFighterHitpoints),
                GemHitpoints = ValueWithAddedSmallVariance(Settings.MinimumGemHitpoints),

                WeaponEnergy = ValueWithAddedSmallVariance(Settings.MinimumWeaponEnergy),
                WeaponEnergyDrainedPerShot = ValueWithAddedSmallVariance(Settings.MinimumWeaponEnergyDrainedPerShot),
                WeaponEnergyChargedPerTurn = ValueWithAddedSmallVariance(Settings.MinimumWeaponEnergyChargedPerTurn),
                WeaponDamagePerShot = ValueWithAddedSmallVariance(Settings.MinimumWeaponDamagePerShot),

                RangeOfSight = ValueWithAddedBigVariance(Settings.MinimumRangeOfSight),
            };

            Rules = rules;

            Match = new Match(this);

            if (Random.Next(2) == 0) {
                Brains[Team.Left] = BrainA;
                Brains[Team.Right] = BrainB;
            } else {
                Brains[Team.Right] = BrainA;
                Brains[Team.Left] = BrainB;
            }

            foreach (var team in Teams) {
                var teamGemLocation = Match.Gems[team].Location;
                var enemyGemLocation = Match.Gems[team == Team.Left ? Team.Right : Team.Left].Location;
                Brains[team].NotifyNewMatch(team, Rules, Random, teamGemLocation, enemyGemLocation, Match.Field.Map.Clone());
            }

            IsPaused = true;
            ShouldPauseNextTurn = false;
            ReverseAnimation = false;

            Match.NotifyBrainsAboutNewTurn();

            Match.AddCurrentStateToHistory();
        }

        public void Start() {
            animationTimeProgress = TimeSpan.Zero;
            currentTime = DateTime.Now;
            timer = new Timer((obj) => {
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                    Tick();
                });
            }, null, TimeSpan.Zero, TimeSpan.FromSeconds(1 / 60.0));
        }

        public void Stop() {
            timer.Dispose();
            timer = null;
            calculatingTick = false;
        }

        public void Tick() {
            if (calculatingTick == false) {
                calculatingTick = true;

                if (ReverseAnimation) {
                    animationTimeProgress -= DateTime.Now - currentTime;
                } else {
                    animationTimeProgress += DateTime.Now - currentTime;
                }

                var overallTurnDuration = turnDuration + interTurnDuration;

                if (animationTimeProgress >= overallTurnDuration) {
                    if (IsPaused || (Match.MatchHasEnded && Match.CurrentTurn >= Match.History.Count - 1)) {
                        animationTimeProgress = overallTurnDuration;
                    } else {
                        if (ShouldPauseNextTurn) {
                            IsPaused = true;
                            ShouldPauseNextTurn = false;
                        }

                        if (Match.CurrentTurn >= Match.History.Count - 1) {
                            Match.SetNextTurn();
                        } else {
                            Match.SetNextHistoryTurn();
                        }
                        animationTimeProgress -= overallTurnDuration;
                        UpdateUIForNewTurn?.Invoke();
                    }
                } else if (ReverseAnimation && animationTimeProgress <= TimeSpan.Zero) {
                    if (Match.CurrentTurn > 0) {
                        Match.SetPreviousHistoryTurn();
                        ReverseAnimation = false;
                        animationTimeProgress = overallTurnDuration;
                        UpdateUIForNewTurn?.Invoke();
                    }
                }

                currentTime = DateTime.Now;
                UpdateUI?.Invoke();
                calculatingTick = false;
            }
        }

        public void DecreaseTurnDuration() {
            TurnDuration /= 1.5;
            if (TurnDuration < minTurnDuration) TurnDuration = minTurnDuration;
            TurnDuration = TurnDuration;
        }

        public void IncreaseTurnDuration() {
            TurnDuration *= 1.5;
            if (TurnDuration > maxTurnDuration) TurnDuration = maxTurnDuration;
            TurnDuration = TurnDuration;
        }

        public void SkipAnimation() {
            var overallTurnDuration = turnDuration + interTurnDuration;
            animationTimeProgress = overallTurnDuration;
        }

        public Action UpdateUI;
        public Action UpdateUIForNewTurn;
        public Action<Brain> BrainDidWin;

        public float AnimationProgress() {
            var progress = (float)(animationTimeProgress.TotalMilliseconds / turnDuration.TotalMilliseconds);
            return progress > 1 ? 1 : progress;
        }

        public float AnimationEaseProgress() {
            var animationProgress = AnimationProgress();
            return (animationProgress * animationProgress) * (3.0f - 2.0f * animationProgress);
        }
    }
}
