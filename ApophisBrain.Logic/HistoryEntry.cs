﻿using System.Collections.Generic;
using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public class HistoryEntry {
        public Dictionary<Team, Fighter[]> Fighters = new Dictionary<Team, Fighter[]>();
        public Dictionary<Team, Gem> Gems = new Dictionary<Team, Gem>();
        public Dictionary<Team, Map<string>> InfoMaps = new Dictionary<Team, Map<string>>();
    }
}
