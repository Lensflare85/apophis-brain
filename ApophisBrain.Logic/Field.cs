﻿using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public class Field {
        public Map<FieldCell> Map;

        public Field(Game game) {
            var columns = game.Rules.FieldColumns;
            var rows = game.Rules.FieldRows;

            Map = new Map<FieldCell>(columns, rows, FieldCell.Ground, FieldCell.None);

            int remainingWalls = game.Rules.NumberOfWalls;
            while (remainingWalls > 0) {
                bool widthIsOdd = columns % 2 == 1;

                int x = game.Random.Next(columns / 2 + (widthIsOdd ? 1 : 0));
                int y = game.Random.Next(rows);

                var wall = FieldCell.Wall;
                if(Map[x, y] != wall) {
                    Map[x, y] = wall;
                    if (widthIsOdd && x == columns / 2) {
                        remainingWalls -= 1;
                    } else {
                        Map[columns - 1 - x, y] = wall;
                        remainingWalls -= 2;
                    }
                }
            }

            if (columns % 2 == 0) {
                Map[columns / 2, rows / 2] = FieldCell.Wall;
                Map[columns / 2 - 1, rows / 2] = FieldCell.Wall;
            } else {
                Map[columns / 2, rows / 2] = FieldCell.Wall;
            }
        }

        public bool IsPassable(int x, int y) {
            return Map[x, y] == FieldCell.Ground;
        }

        public bool IsPassable(IntVector2 location) {
            return IsPassable(location.X, location.Y);
        }
    }
}
