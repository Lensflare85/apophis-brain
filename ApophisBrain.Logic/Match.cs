﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public class Match {
        private Game game;
        public Field Field;

        Dictionary<Team, Fighter[]> fighters = new Dictionary<Team, Fighter[]>();
        public Dictionary<Team, Fighter[]> Fighters {
            get {
                if (CurrentTurn >= History.Count - 1) {
                    return fighters;
                } else {
                    return History[CurrentTurn].Fighters;
                }
            }
        }

        Dictionary<Team, Gem> gems = new Dictionary<Team, Gem>();
        public Dictionary<Team, Gem> Gems {
            get {
                if (CurrentTurn >= History.Count - 1) {
                    return gems;
                } else {
                    return History[CurrentTurn].Gems;
                }
            }
        }

        Dictionary<Team, Map<string>> infoMaps = new Dictionary<Team, Map<string>>();
        public Dictionary<Team, Map<string>> InfoMaps {
            get {
                if (CurrentTurn >= History.Count - 1) {
                    return infoMaps;
                } else {
                    return History[CurrentTurn].InfoMaps;
                }
            }
        }

        public List<HistoryEntry> History = new List<HistoryEntry>();

        public int CurrentTurn { get; private set; } = 0;

        private int fightersPlaced = 0;

        public List<Team> LoserTeams = new List<Team>();

        public bool MatchHasEnded = false;

        public Match(Game game) {
            this.game = game;
            Field = new Field(game);

            fightersPlaced = 0;

            var nameGenerator = new NameGenerator(game.Random);

            var gemLeft = new Gem(Team.Left) { Alive = true };
            var gemRight = new Gem(Team.Right) { Alive = true };

            var gemLocation = new IntVector2(game.Rules.FieldColumns / 8, game.Rules.FieldRows / 2);
            gemLeft.Location = gemLocation;
            Field.Map[gemLocation] = FieldCell.Ground;
            gemLocation.X = game.Rules.FieldColumns - 1 - gemLocation.X;
            gemRight.Location = gemLocation;
            Field.Map[gemLocation] = FieldCell.Ground;

            gems[Team.Left] = gemLeft;
            gems[Team.Right] = gemRight;

            foreach (var team in game.Teams) {
                fighters[team] = new Fighter[game.Rules.FightersPerTeam];

                gems[team].HitPoints = game.Rules.GemHitpoints;
                gems[team].Alive = true;
            }

            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                var fighterTeamLeft = new Fighter(nameGenerator.Next(6), Team.Left);
                var fighterTeamRight = new Fighter(nameGenerator.Next(6), Team.Right);

                var location = PickRandomStartLocationForFighter();
                fighterTeamLeft.Location = location;

                location.X = game.Rules.FieldColumns - 1 - location.X;
                fighterTeamRight.Location = location;

                fighters[Team.Left][i] = fighterTeamLeft;
                fighters[Team.Right][i] = fighterTeamRight;

                fightersPlaced += 1;
            }

            foreach (var team in game.Teams) {
                for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                    var fighter = fighters[team][i];
                    fighter.HitPoints = game.Rules.FighterHitpoints;
                    fighter.Alive = true;
                    fighter.WeaponEnergy = game.Rules.WeaponEnergy / 2;
                }
            }
        }

        private IntVector2 PickRandomStartLocationForFighter() {
            var varianceSize = new IntVector2(game.Rules.FieldColumns / 4, game.Rules.FieldRows / 2);
            var center = new IntVector2(game.Rules.FieldColumns / 4, game.Rules.FieldRows / 2);
            var location = new IntVector2();
            do {
                location = center + new IntVector2(game.Random.Next(varianceSize.X) - varianceSize.X / 2, (game.Random.Next(varianceSize.Y) - varianceSize.Y / 2));
            } while (!SpawnLocationIsFree(location));
            return location;
        }

        private bool SpawnLocationIsFree(IntVector2 location) {
            if (!Field.IsPassable(location)) {
                return false;
            } else {
                if(gems[Team.Left].Location == location) {
                    return false;
                }
                for (int i=0; i<fightersPlaced; ++i) {
                    if(fighters[Team.Left][i].Location == location) {
                        return false;
                    }
                }
                return true;
            }
        }

        public void AddCurrentStateToHistory() {
            var historyEntry = new HistoryEntry();

            Func<Fighter[], Fighter[]> clone = (fighters) => {
                var historyFighters = new Fighter[fighters.Length];
                for (int i = 0; i < fighters.Length; ++i) {
                    historyFighters[i] = (Fighter)fighters[i]?.Clone();
                }
                return historyFighters;
            };

            foreach (var team in game.Teams) {
                historyEntry.Fighters[team] = clone(fighters[team]);
                historyEntry.Gems[team] = (Gem)gems[team].Clone();
                historyEntry.InfoMaps[team] = infoMaps[team]?.Clone();
            }

            History.Add(historyEntry);
        }

        public void NotifyBrainsAboutNewTurn() {
            foreach (var team in game.Teams) {
                var teamFighterInfos = Fighters[team].Select(fighter => fighter.ToTeamFighterInfo()).ToArray();
                var brain = game.Brains[team];
                var infoMap = brain.NotifyNewTurn(CurrentTurn, teamFighterInfos);
                infoMaps[team] = infoMap;
            }
        }

        public void SetNextTurn() {
            CurrentTurn += 1;

            NotifyBrainsAboutNewTurn();

            ExecuteFighterMoves();
            ExecuteWeaponRecharge();
            ExecuteFighterShots();
            ExecuteDeaths();
            ExecuteLookingDirections();

            AddCurrentStateToHistory();

            System.Diagnostics.Debug.WriteLine($"calculated turn: {CurrentTurn}");
        }

        public void SetHistoryTurn(int turn) {
            game.IsPaused = true;

            if (turn < 0) {
                turn = 0;
            }

            if (turn >= History.Count) {
                turn = History.Count;
            }

            CurrentTurn = turn;
        }

        public void SetNextHistoryTurn() {
            CurrentTurn += 1;
        }

        public void SetPreviousHistoryTurn() {
            CurrentTurn -= 1;
        }

        public void StepBack() {
            if (CurrentTurn > 0) {
                game.ReverseAnimation = true;
            }
        }

        private Dictionary<IntVector2, Fighter> fighterLocations;
        private Dictionary<IntVector2, List<Fighter>> newFighterLocations;
        private Dictionary<IntVector2, Fighter> newLivingFighterLocations;
        private Dictionary<Fighter, IntVector2?> movementTargetRequests;
        private Dictionary<Fighter, bool?> fighterWillMoveCache;

        private void ExecuteFighterMoves() {
            fighterLocations = new Dictionary<IntVector2, Fighter>();
            newFighterLocations = new Dictionary<IntVector2, List<Fighter>>();
            newLivingFighterLocations = new Dictionary<IntVector2, Fighter>();
            movementTargetRequests = new Dictionary<Fighter, IntVector2?>();
            fighterWillMoveCache = new Dictionary<Fighter, bool?>();

            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                foreach (var team in game.Teams) {
                    var fighter = fighters[team][i];

                    if (fighter.Alive) {
                        fighterLocations[fighter.Location] = fighter;
                        var fighterAMovementDirectionRequest = game.Brains[team].NextTurnFighterMove(i);
                        if (fighterAMovementDirectionRequest != Direction.None) {
                            movementTargetRequests[fighter] = fighter.Location + fighterAMovementDirectionRequest.ToIntVector2();
                        }
                    }
                }
            }

            RemoveMovementRequestsWithEqualTargetLocations();

            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                foreach (var team in game.Teams) {
                    var fighter = fighters[team][i];

                    if (fighter.Alive) {
                        MoveFighterIfPossible(fighter);
                        newLivingFighterLocations[fighter.Location] = fighter;
                    }

                    var fightersList = newFighterLocations.GetOrDefault(fighter.Location);
                    if (fightersList == null) {
                        newFighterLocations[fighter.Location] = new List<Fighter> { fighter };
                    } else {
                        fightersList.Add(fighter);
                        newFighterLocations[fighter.Location] = fightersList;
                    }
                }
            }
        }

        private void RemoveMovementRequestsWithEqualTargetLocations() {
            var fighters = new Dictionary<IntVector2?, Fighter>();
            var requestsToRemove = new List<Fighter>();
            foreach (var targetLocation in movementTargetRequests) {
                if (fighters.ContainsKey(targetLocation.Value)) {
                    var fighter = fighters[targetLocation.Value];
                    if (!requestsToRemove.Contains(fighter)) {
                        requestsToRemove.Add(fighter);
                    }
                    requestsToRemove.Add(targetLocation.Key);
                } else {
                    fighters[targetLocation.Value] = targetLocation.Key;
                }
            }

            foreach (var requestToRemove in requestsToRemove) {
                movementTargetRequests.Remove(requestToRemove);
            }
        }

        private void MoveFighterIfPossible(Fighter fighter) {
            if (fighter.Alive && FighterWillMove(fighter, fighter)) {
                var targetLocation = movementTargetRequests.GetOrDefault(fighter);
                if (targetLocation != null) fighter.Location = targetLocation.Value;
            }
        }

        private bool FighterWillMove(Fighter fighter, Fighter firstWhoAsked) {
            var cachedResult = fighterWillMoveCache.GetOrDefault(fighter);
            if (cachedResult != null) {
                return cachedResult.Value;
            }

            bool willMove;
            if (fighter.Alive) {
                //check if the fighter wants to move:
                var requestedTargetLocation = movementTargetRequests.GetOrDefault(fighter);
                if (requestedTargetLocation != null) {
                    //the fighter wants to move.

                    //check if the target location is passable:
                    if (Field.IsPassable(requestedTargetLocation.Value)) {
                        //the target location is passable.

                        //check if a gem is at target location
                        if (gems[Team.Left].Location == requestedTargetLocation || gems[Team.Right].Location == requestedTargetLocation) {
                            willMove = false;
                        } else {
                            //check if there is another fighter at the target location:
                            var fighterOnTargetLocation = fighterLocations.GetOrDefault(requestedTargetLocation.Value);
                            if (fighterOnTargetLocation != null && fighterOnTargetLocation.Alive) {
                                //there is another fighter at the target location and he is alive.

                                //check if fighters are asking questions in a cycle:
                                if (fighterOnTargetLocation == firstWhoAsked) {
                                    //cycle detected => all fighters in the cycle are not allowed to move.
                                    willMove = false;
                                } else {
                                    //ask the fighter on the target location if he will move:
                                    willMove = FighterWillMove(fighterOnTargetLocation, firstWhoAsked);
                                }
                            } else {
                                //there is no fighter at the target location. maybe a dead one, but that's ok.
                                willMove = true;
                            }
                        }
                    } else {
                        //the target location is not passable.
                        willMove = false;
                    }
                } else {
                    //fighter doesn't want to move.
                    willMove = false;
                }
            } else {
                //fighter is dead.
                willMove = false;
            }

            fighterWillMoveCache[fighter] = willMove;
            return willMove;
        }

        private void ExecuteWeaponRecharge() {
            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                foreach (var team in game.Teams) {
                    var fighter = fighters[team][i];
                    fighter.WeaponEnergy += game.Rules.WeaponEnergyChargedPerTurn;
                    if (fighter.WeaponEnergy > game.Rules.WeaponEnergy) {
                        fighter.WeaponEnergy = game.Rules.WeaponEnergy;
                    }
                }
            }
        }

        private void ExecuteFighterShots() {
            for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                foreach (var team in game.Teams) {
                    var fighter = fighters[team][i];

                    fighter.HitLocation = null;

                    fighter.ShootingDirection = fighter.Alive && fighter.WeaponEnergy >= game.Rules.WeaponEnergyDrainedPerShot ? game.Brains[team].NextTurnFighterShoot(i) : Direction.None;

                    if (fighter.ShootingDirection != Direction.None) {
                        fighter.WeaponEnergy -= game.Rules.WeaponEnergyDrainedPerShot;
                        ExecuteShotFromFighter(fighter);
                    }
                }
            }
        }

        private void ExecuteShotFromFighter(Fighter fighter) {
            var shotVector = fighter.ShootingDirection.ToIntVector2();
            var shotLocation = fighter.Location;
            Fighter hitFighter;
            Gem hitGem;
            do {
                shotLocation += shotVector;
            } while (LocationIsPassableByShots(shotLocation, out hitFighter, out hitGem));

            if (hitFighter != null) {
                hitFighter.TakeDamage(game.Rules.WeaponDamagePerShot);
            }
            if (hitGem != null) {
                hitGem.TakeDamage(game.Rules.WeaponDamagePerShot);
            }

            fighter.HitLocation = shotLocation;
        }

        private bool LocationIsPassableByShots(IntVector2 location, out Fighter fighter, out Gem gem) {
            if (!Field.IsPassable(location)) {
                fighter = null;
                gem = null;
                return false;
            } else {
                foreach (var team in game.Teams) {
                    if (gems[team].Location == location) {
                        fighter = null;
                        gem = gems[team];
                        return false;
                    }
                }

                var fighterAtLocation = newLivingFighterLocations.GetOrDefault(location);
                if (fighterAtLocation != null) {
                    fighter = fighterAtLocation;
                    gem = null;
                    if (fighterAtLocation.Alive) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    fighter = null;
                    gem = null;
                    return true;
                }
            }
        }

        private void ExecuteLookingDirections() {
            foreach (var team in game.Teams) {
                for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                    var fighter = fighters[team][i];
                    fighter.LookingDirection = game.Brains[team].NextTurnFighterLook(i);

                    IntVector2 endOfSightLocation;
                    var entities = EntitiesInSightOfFighter(fighter, out endOfSightLocation);

                    fighter.SeesEntities = entities;
                    fighter.EndOfSightLocation = endOfSightLocation;
                }
            }
        }

        private IEnumerable<Entity> EntitiesInSightOfFighter(Fighter fighter, out IntVector2 endOfSightLocation) {
            var location = fighter.Location;
            var entitiesInSight = new List<Entity>();

            int distance = 0;

            if (fighter.Alive && fighter.LookingDirection != Direction.None) {
                bool canSeeThrough = true;
                var lookingDirectionVector = fighter.LookingDirection.ToIntVector2();
                do {
                    location += lookingDirectionVector;
                    distance += 1;

                    if (!Field.IsPassable(location)) {
                        canSeeThrough = false;
                        break;
                    }

                    foreach (var team in game.Teams) {
                        var gem = gems[team];
                        if (gem.Location == location) {
                            canSeeThrough = false;
                            entitiesInSight.Add(gem);
                            break;
                        }
                    }

                    var fightersList = newFighterLocations.GetOrDefault(location);
                    if (fightersList != null) {
                        foreach (var fighterAtLocation in fightersList) {
                            if (fighterAtLocation.Alive) {
                                canSeeThrough = false;
                            }
                            entitiesInSight.Add(fighterAtLocation);
                        }
                    }

                } while (canSeeThrough && distance < game.Rules.RangeOfSight);
            }
            endOfSightLocation = location;
            return entitiesInSight;
        }

        private void ExecuteDeaths() {
            foreach (var team in game.Teams) {
                for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                    var fighter = fighters[team][i];
                    if (fighter.Alive && fighter.HitPoints <= 0) {
                        fighter.Alive = false;
                    }
                }

                var gem = gems[team];
                if (gem.Alive && gem.HitPoints <= 0) {
                    gem.Alive = false;
                }
            }

            bool matchShouldEnd = false;

            foreach (var team in game.Teams) {
                if (!gems[team].Alive) {
                    LoserTeams.Add(team);
                    matchShouldEnd = true;
                } else {
                    bool allFightersDead = true;
                    for (int i = 0; i < game.Rules.FightersPerTeam; ++i) {
                        var fighter = fighters[team][i];
                        if (fighter.Alive) {
                            allFightersDead = false;
                            break;
                        }
                    }
                    if (allFightersDead) {
                        LoserTeams.Add(team);
                        matchShouldEnd = true;
                    }
                }
            }

            if (matchShouldEnd) {
                MatchHasEnded = true;

                foreach (var team in game.Teams) {
                    if (!LoserTeams.Contains(team)) {
                        var brain = game.Brains[team];
                        if (game.BrainPool.FirstBrainType == brain.GetType()) {
                            game.ScoreBrainA += 1;
                        } else {
                            game.ScoreBrainB += 1;
                        }
                        game.BrainDidWin?.Invoke(brain);
                    }
                }
            }
        }
    }
}

