﻿using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public class Gem : Entity {
        public Gem(Team team) : base(team) {
            
        }

        public override Entity Clone() {
            return new Gem(Team) {
                Alive = Alive,
                Location = Location,
                HitPoints = HitPoints,
            };
        }

        public override EntityInfo ToEntityInfo() {
            return new GemInfo(Team, Alive, HitPoints, Location);
        }
    }
}
