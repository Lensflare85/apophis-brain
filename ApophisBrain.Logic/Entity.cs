﻿using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public abstract class Entity {
        public readonly Team Team;
        public IntVector2 Location;
        public bool Alive;
        public int HitPoints { get; set; }

        public Entity(Team team) {
            Team = team;
        }

        public void TakeDamage(int damage) {
            HitPoints -= damage;
            if (HitPoints < 0) {
                HitPoints = 0;
            }
        }

        public abstract Entity Clone();

        public abstract EntityInfo ToEntityInfo();
    }
}
