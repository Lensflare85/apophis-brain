﻿using System;
using ApophisBrain.BrainImplementation;

namespace ApophisBrain.Logic {
    public static class Settings {
        public const int MinimumFieldColumns = 30;
        public const int MinimumFieldRows = 15;
        
        public const int MinimumFightersPerTeam = 5;

        public const int MinimumGemHitpoints = 300;
        public const int MinimumFighterHitpoints = 70;

        public const int MinimumRangeOfSight = 5;

        public const int MinimumWeaponEnergy = 30;
        public const int MinimumWeaponEnergyDrainedPerShot = 10;
        public const int MinimumWeaponEnergyChargedPerTurn = 2;
        public const int MinimumWeaponDamagePerShot = 25;       
    }
}
