﻿using System.Collections.Generic;
using System.Linq;
using ApophisBrain.Shared;

namespace ApophisBrain.Logic {
    public class Fighter : Entity {
        public readonly string Name;

        public Direction LookingDirection = Direction.None;
        public IEnumerable<Entity> SeesEntities = Enumerable.Empty<Entity>();
        public IntVector2 EndOfSightLocation;

        public Direction ShootingDirection = Direction.None;
        public IntVector2? HitLocation;

        public int WeaponEnergy;

        public Fighter(string name, Team team) : base(team) {
            Name = name;
        }

        public override string ToString() {
            return Name;
        }

        public override Entity Clone() {
            return new Fighter(Name, Team) {
                Alive = Alive,
                Location = Location,
                LookingDirection = LookingDirection,
                SeesEntities = SeesEntities.Select(entity => entity).ToArray(),
                EndOfSightLocation = EndOfSightLocation,
                ShootingDirection = ShootingDirection,
                HitLocation = HitLocation,
                HitPoints = HitPoints,
                WeaponEnergy = WeaponEnergy,
            };
        }

        public override EntityInfo ToEntityInfo() {
            return new FighterInfo(Name, Team, Alive, HitPoints, Location);
        }

        public TeamFighterInfo ToTeamFighterInfo() {
            return new TeamFighterInfo(Name, Team, Alive, HitPoints, Location, WeaponEnergy, LookingDirection, SeesEntities.Select(entity => entity.ToEntityInfo()).ToArray(), ShootingDirection);
        }
    }
}
